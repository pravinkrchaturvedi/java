public class StringBasic {

	public static void main(String[] args) {
		// creating string by java string literal

		String Name = "Pravin";
		// creating java string by new keyword
		String NickName = new String("ajay");

		// array of characters
		char[] serName = { 'c', 'h', 'a', 't', 'u', 'r', 'v', 'e', 'd', 'i' };
		// converting char array to string
		System.out.println(Name + " " + new String(serName));
		System.out.println(" with Nick " + NickName);

	}
}

/*
 * Theory : What is String in java
 * 
 * Generally, string is a sequence of characters. But in java, string is an
 * object that represents a sequence of characters. String class is used to
 * create string object.
 * 
 * Note: String objects are stored in a special memory area known as string constant pool.
 */
