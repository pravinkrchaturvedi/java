public class ImmutableString {
	public static void main(String[] args) {
		String s = "Pravin";
		s.concat(" Chaturvedi");
		System.out.println(s + " < its not full name ");
		// to remember : immutable objects ??
		/*
		 * but a new object is created with full name. That is why string is
		 * known as immutable
		 */

		String Fullname = s.concat(" Chaturvedi");
		System.out.println(Fullname);

		s = s.concat(" Chaturvedi");
		System.out.println(s);
	}
}

/*
 * Immutable String in Java : In java, string objects are immutable. Immutable
 * simply means unmodifiable or unchangeable.
 * 
 * ---------- Once string object is created its data or state can't be changed
 * but a new string object is created.
 */

/*
 * 
 * Why string objects are immutable in java?
 * 
 * Because java uses the concept of string literal.Suppose there are 5 reference
 * variables,all referes to one object "sachin".If one reference variable
 * changes the value of the object, it will be affected to all the reference
 * variables. That is why string objects are immutable in java.
 */
