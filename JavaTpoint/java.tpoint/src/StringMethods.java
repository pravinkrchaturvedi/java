public class StringMethods {
	public static void main(String[] args) {

		String s = "Praveen";
		String toTrim = "   raj " + s;
		caseConve(s);
		eliminateSpace(toTrim);
		isStartOrEndWith( s);
	}

	/*
	 * The java string toUpperCase() method converts this string into uppercase
	 * letter and string toLowerCase() method into lowercase letter.
	 */
	public static void caseConve(String s) {

		System.out.println(s.toUpperCase());// SACHIN
		System.out.println(s.toLowerCase());// sachin
		System.out.println(s);// Sachin(no change in original)

	}

	/*
	 * The string trim() method eliminates white spaces before and after string.
	 */
	public static void eliminateSpace(String s) {
		System.out.println(s); 
		System.out.println(s.trim()); 
	}

	
	/*
	 String startsWith() and endsWith() method
*/
	public static void isStartOrEndWith(String s){
		
		 System.out.println(s.startsWith("Sa"));//true  
		 System.out.println(s.endsWith("n"));//true  
	}
	 	
	
	
}
